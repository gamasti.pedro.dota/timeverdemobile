import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'models/usuario/usuario.dart';

class DatabaseService {
  // Singleton pattern
  static final DatabaseService _databaseService = DatabaseService._internal();
  factory DatabaseService() => _databaseService;
  DatabaseService._internal();

  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await initDatabase();
    return _database!;
  }

  Future<Database> initDatabase() async {
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, 'patinhas.db');
    return await openDatabase(
      path,
      onCreate: _onCreate,
      version: 1,
      onConfigure: (db) async => await db.execute('PRAGMA foreign_keys = ON'),
    );
  }

  Future<void> _onCreate(Database db, int version) async {
    try {
      Usuario.nomeTabela;
      List<String> listaCriacaoDB = [
        Usuario.scriptCriacaoBanco,
      ];
      for (var script in listaCriacaoDB) {
        await db.execute(script);
      }
    } catch (_) {}
  }

  Future<int> rawInsert(String query) async {
    final db = await _databaseService.database;
    return await db.rawInsert(query);
  }

  Future<int> rawUpdate(String query) async {
    final db = await _databaseService.database;
    return await db.rawUpdate(query);
  }

  Future<int> rawDelete(String query) async {
    final db = await _databaseService.database;
    return await db.rawDelete(query);
  }

  Future<List<Map<String, Object?>>> rawQuery(String query) async {
    final db = await _databaseService.database;
    return await db.rawQuery(query);
  }
}
