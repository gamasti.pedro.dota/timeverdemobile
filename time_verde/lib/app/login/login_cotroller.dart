import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  late TextEditingController inputLogin;
  late TextEditingController inputSenha;
  late RxBool isLoading;

  LoginController() {
    inputLogin = TextEditingController();
    inputSenha = TextEditingController();
    isLoading = false.obs;
  }

  // Future<void> btnAvancar() {}
}
