import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:time_verde/app/shared/backgrounds/background_configuracao.dart';
import 'package:time_verde/app/shared/widgets/button_widget.dart';
import 'package:time_verde/app/shared/widgets/text_field_widget.dart';
import 'package:time_verde/app/shared/widgets/text_widget.dart';
import 'package:time_verde/helpers/assets_aplicativo.dart';
import 'package:time_verde/helpers/cores_aplicativo.dart';
import 'package:time_verde/helpers/fontes_aplicativo.dart';
import '../MenuPrincipal/menu_principal_page.dart';
import 'login_cotroller.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginController controller = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    RxBool checkbox = false.obs;
    return BackgroundConfiguracao(
      child: ListView(
        shrinkWrap: true,
        children: [
          Image.asset(AssetsAplicativo.imagemLogin, width: 100.w, fit: BoxFit.fitWidth),
          SizedBox(height: 1.h),
          TextWidget(
            "Crie uma conta ou clique em entrar para encontrar seu novo amigo!",
            fontSize: FontesAplicativo.fonteDescricao,
            color: CoresAplicativo.corPadraoAzulEscuro,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 2.h),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 5.w),
            child: TextFieldWidget(
              hintText: "Usuário",
              controller: controller.inputLogin,
            ),
          ),
          SizedBox(height: 2.5.h),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 5.w),
            child: TextFieldWidget(
              hintText: "Senha",
              controller: controller.inputSenha,
            ),
          ),
          SizedBox(height: 2.h),
          Padding(
            padding: EdgeInsets.only(left: 5.w),
            child: Obx(() => Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 2.5.h,
                      width: 2.5.h,
                      child: Checkbox(
                          value: checkbox.value,
                          fillColor: MaterialStatePropertyAll<Color>(CoresAplicativo.corPadraoAzulEscuro),
                          activeColor: CoresAplicativo.corPadraoAzulEscuro,
                          onChanged: (_) {
                            checkbox.value = !checkbox.value;
                          }),
                    ),
                    SizedBox(width: 2.w),
                    TextWidget(
                      "Lembrar usuário e senha",
                      color: CoresAplicativo.corPadraoAzulEscuro,
                      fontSize: FontesAplicativo.fonteMinuscula,
                    )
                  ],
                )),
          ),
          SizedBox(height: 3.h),
          ButtonWidget(
            text: 'AVANÇAR',
            onPressed: () => Get.to(() => const MenuPrincipalPage()),
          ),
          SizedBox(height: 1.5.h),
          ButtonWidget(
            text: 'CADASTRO',
            outlined: true,
            onPressed: () => Get.to(() => const MenuPrincipalPage()),
          )
        ],
      ),
    );
  }
}
