import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import '../../../helpers/cores_aplicativo.dart';

class BackgroundConfiguracao extends StatelessWidget {
  final Widget? child;
  final double? paddingHorizontal;
  final bool extendBody;
  const BackgroundConfiguracao({super.key, this.child, this.paddingHorizontal, this.extendBody = false});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        extendBody: extendBody,
        backgroundColor: CoresAplicativo.corBackgroundPadrao,
        resizeToAvoidBottomInset: true,
        body: Padding(padding: EdgeInsets.symmetric(horizontal: 0.w), child: child),
      ),
    );
  }
}
