import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:time_verde/app/shared/widgets/text_widget.dart';
import 'package:time_verde/helpers/cores_aplicativo.dart';
import 'package:time_verde/helpers/fontes_aplicativo.dart';

class TextFieldWidget extends StatelessWidget {
  final String? labelText;
  final String? hintText;
  final TextEditingController? controller;
  TextFieldWidget({super.key, this.labelText, this.hintText, this.controller});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (labelText != null)
          Align(
            alignment: Alignment.centerLeft,
            child: TextWidget(
              labelText!,
              fontSize: FontesAplicativo.fontePequena,
              color: CoresAplicativo.corFontePadrao,
            ),
          ),
        if (labelText != null) SizedBox(height: 0.5.h),
        TextField(
            controller: controller,
            decoration: InputDecoration(
              hintText: hintText,
              contentPadding: EdgeInsets.symmetric(vertical: 2.25.h, horizontal: 3.w),
              hintStyle: TextStyle(
                color: CoresAplicativo.corFonteHintText.withOpacity(0.5),
                fontSize: FontesAplicativo.fonteMuitoPequena,
              ),
              filled: true,
              fillColor: Colors.grey.withOpacity(0.1),
              enabledBorder: outlineInputBorder,
              focusedBorder: outlineInputBorder,
              errorBorder: outlineInputBorder,
              border: outlineInputBorder,
            ))
      ],
    );
  }

  OutlineInputBorder outlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(1.h)),
    borderSide: BorderSide(width: 0.5, color: Colors.grey.withOpacity(0.1)),
  );
}
