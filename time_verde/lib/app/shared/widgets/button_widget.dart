import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:time_verde/app/shared/widgets/text_widget.dart';
import 'package:time_verde/helpers/cores_aplicativo.dart';

class ButtonWidget extends StatelessWidget {
  final String text;
  final void Function()? onPressed;
  final bool outlined;
  const ButtonWidget({super.key, required this.text, this.onPressed, this.outlined = false});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(
            outlined ? CoresAplicativo.corBackgroundPadrao : CoresAplicativo.corPadraoAzulEscuro),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            side: BorderSide(color: CoresAplicativo.corPadraoAzulEscuro, width: 2),
            borderRadius: BorderRadius.circular(3.h),
          ),
        ),
        minimumSize: MaterialStateProperty.all<Size>(
          Size(70.w, 5.5.h),
        ),
      ),
      child: TextWidget(
        text,
        color: outlined ? CoresAplicativo.corPadraoAzulEscuro : CoresAplicativo.corBackgroundPadrao,
      ),
    );
  }
}
