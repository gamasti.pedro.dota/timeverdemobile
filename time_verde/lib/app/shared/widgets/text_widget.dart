import 'package:flutter/material.dart';
import 'package:time_verde/helpers/cores_aplicativo.dart';
import 'package:time_verde/helpers/fontes_aplicativo.dart';

class TextWidget extends StatelessWidget {
  final String text;
  final Color? color;
  final double? fontSize;
  final FontWeight? fontWeight;
  final FontStyle? fontStyle;
  final double? letterSpacing;
  final TextDecoration? decoration;
  final int? maxLines;
  final double? height;
  final TextAlign textAlign;

  const TextWidget(this.text,
      {super.key,
      this.color,
      this.fontSize,
      this.fontWeight,
      this.fontStyle,
      this.letterSpacing,
      this.decoration,
      this.maxLines,
      this.height,
      this.textAlign = TextAlign.start
      // this.decorationColor,
      // this.decorationStyle,
      // this.decorationThickness,
      });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          color: color ?? CoresAplicativo.corPreto,
          fontSize: fontSize ?? FontesAplicativo.fontePadrao,
          fontWeight: fontWeight,
          fontStyle: fontStyle,
          letterSpacing: letterSpacing,
          decoration: decoration,
          height: height),
      textAlign: textAlign,
      maxLines: maxLines,
    );
  }
}
