import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';
import 'package:time_verde/app/shared/widgets/text_widget.dart';
import 'package:time_verde/helpers/fontes_aplicativo.dart';

import '../../helpers/assets_aplicativo.dart';
import '../../helpers/cores_aplicativo.dart';
import '../../models/animal/animal.dart';

class AnimalDetalhePage extends StatefulWidget {
  final Animal animal;
  const AnimalDetalhePage({super.key, required this.animal});

  @override
  State<AnimalDetalhePage> createState() => _AnimalDetalhePageState();
}

class _AnimalDetalhePageState extends State<AnimalDetalhePage> {
  RxInt fotoAtual = 0.obs;
  PageController imageController = PageController(initialPage: 0, viewportFraction: 1.1);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      backgroundColor: CoresAplicativo.corBackgroundPadrao,
      resizeToAvoidBottomInset: true,
      floatingActionButton: Padding(
        padding: EdgeInsets.only(bottom: 3.h, right: 1.h),
        child: Container(
          height: 7.5.h,
          width: 7.5.h,
          child: FittedBox(
            child: FloatingActionButton(
              backgroundColor: CoresAplicativo.corBackgroundPadrao,
              onPressed: () {},
              child: Padding(
                padding: EdgeInsets.all(1.5.h),
                child: Image.asset(
                  AssetsAplicativo.iconCore,
                ),
              ),
            ),
          ),
        ),
      ),
      body: SafeArea(
        child: ListView(
          children: [
            Container(
              height: 1.h,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [
                  CoresAplicativo.corCachorro.withOpacity(0.25),
                  CoresAplicativo.corCachorro,
                  CoresAplicativo.corGato,
                  CoresAplicativo.corGato.withOpacity(0.25),
                ],
              )),
            ),
            Padding(
              padding: EdgeInsets.only(left: 4.w, right: 4.w, top: 2.h),
              child: Column(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(bottom: 1.h),
                      child: GestureDetector(
                          child: Icon(
                            Icons.arrow_back_outlined,
                            size: 4.h,
                            color: CoresAplicativo.corPadraoAzulEscuro,
                          ),
                          onTap: () => Get.back()),
                    ),
                    GestureDetector(
                        child: Image.asset(
                          AssetsAplicativo.logoTop,
                          // color: CoresAplicativo.corPreto,
                          width: 35.w,
                        ),
                        onTap: () {}),
                    SizedBox(
                      width: 4.h,
                    )
                  ],
                ),
                SizedBox(height: 2.h),
                Stack(
                  children: [
                    Center(
                      child: SizedBox(
                        height: 39.h,
                        width: 90.w,
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(4.h)),
                          child: PageView.builder(
                            controller: imageController,
                            onPageChanged: (int value) {
                              fotoAtual.value = value;
                            },
                            itemCount: widget.animal.listaImagem!.length,
                            itemBuilder: (context, index) => ClipRRect(
                                child: Image(image: widget.animal.listaImagem![index].image, fit: BoxFit.fitWidth)),
                          ),
                        ),
                      ),
                    ),
                    Center(
                        child: Padding(
                      padding: EdgeInsets.only(top: 35.5.h),
                      child: SizedBox(
                        height: 1.5.h,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: widget.animal.listaImagem!.length,
                          itemBuilder: (context, index) => Obx(
                            () => Container(
                              height: 1.5.h,
                              width: widget.animal.listaImagem!.length == 1
                                  ? 1.5.h
                                  : fotoAtual.value == index
                                      ? 4.h
                                      : 1.5.h,
                              margin: EdgeInsets.symmetric(horizontal: 1.w),
                              decoration: BoxDecoration(
                                color: fotoAtual >= index
                                    ? CoresAplicativo.corPadraoAzulEscuro
                                    : CoresAplicativo.corBackgroundPadrao,
                                borderRadius: BorderRadius.all(Radius.circular(25.w)),
                              ),
                            ),
                          ),
                        ),
                      ),
                    )),
                  ],
                ),
                SizedBox(height: 1.5.h),
                Align(
                    alignment: Alignment.centerLeft,
                    child: TextWidget(
                      widget.animal.nome,
                      fontWeight: FontWeight.w600,
                      fontSize: FontesAplicativo.fonteNomeAnimalDetalhe,
                      color: CoresAplicativo.corPadraoAzulEscuro,
                    )),
                SizedBox(height: 0.5.h),
                Align(
                    alignment: Alignment.centerLeft,
                    child: TextWidget(
                      widget.animal.bio,
                      height: 1.3,
                      maxLines: 6,
                      fontSize: FontesAplicativo.fontePequena,
                      color: CoresAplicativo.corFonteDescricao,
                    )),
                SizedBox(height: 1.5.h),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 0.3.h),
                          decoration: BoxDecoration(
                            color: CoresAplicativo.corPadraoAmarelo.withOpacity(0.5),
                            borderRadius: BorderRadius.all(Radius.circular(25.w)),
                          ),
                          child: Row(
                            children: [
                              Icon(
                                widget.animal.genero == Genero.feminino ? Icons.female : Icons.male,
                                size: 2.h,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                              TextWidget(
                                widget.animal.genero.name.toString(),
                                color: CoresAplicativo.corPadraoAzulEscuro,
                                fontSize: FontesAplicativo.fontePequena,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: 3.w),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 0.3.h),
                          decoration: BoxDecoration(
                            color: CoresAplicativo.corPadraoAmarelo.withOpacity(0.5),
                            borderRadius: BorderRadius.all(Radius.circular(25.w)),
                          ),
                          child: Row(
                            children: [
                              TextWidget(
                                widget.animal.raca,
                                color: CoresAplicativo.corPadraoAzulEscuro,
                                fontSize: FontesAplicativo.fontePequena,
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
                SizedBox(height: 1.5.h),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Row(children: [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 0.3.h),
                        decoration: BoxDecoration(
                          color: CoresAplicativo.corPadraoAmarelo.withOpacity(0.5),
                          borderRadius: BorderRadius.all(Radius.circular(25.w)),
                        ),
                        child: Row(
                          children: [
                            TextWidget(
                              "Porte " + widget.animal.porte,
                              color: CoresAplicativo.corPadraoAzulEscuro,
                              fontSize: FontesAplicativo.fontePequena,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 3.w),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 0.3.h),
                        decoration: BoxDecoration(
                          color: CoresAplicativo.corPadraoAmarelo.withOpacity(0.5),
                          borderRadius: BorderRadius.all(Radius.circular(25.w)),
                        ),
                        child: Row(
                          children: [
                            TextWidget(
                              "Cor " + widget.animal.cor,
                              color: CoresAplicativo.corPadraoAzulEscuro,
                              fontSize: FontesAplicativo.fontePequena,
                            ),
                          ],
                        ),
                      ),
                    ])),
                SizedBox(height: 1.5.h),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 0.3.h),
                      decoration: BoxDecoration(
                        color: CoresAplicativo.corPadraoAmarelo.withOpacity(0.5),
                        borderRadius: BorderRadius.all(Radius.circular(25.w)),
                      ),
                      child: TextWidget(
                        "Ultima visita ao vet.: " +
                            DateFormat('dd-MM-yyyy').format(widget.animal.ultimaVisitaVet!).replaceAll("-", "/"),
                        color: CoresAplicativo.corPadraoAzulEscuro,
                        fontSize: FontesAplicativo.fontePequena,
                      ),
                    )),
                SizedBox(height: 1.5.h),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 0.3.h),
                      decoration: BoxDecoration(
                        color: CoresAplicativo.corPadraoAmarelo.withOpacity(0.5),
                        borderRadius: BorderRadius.all(Radius.circular(25.w)),
                      ),
                      child: TextWidget(
                        "Nenhuma Patologia",
                        color: CoresAplicativo.corPadraoAzulEscuro,
                        fontSize: FontesAplicativo.fontePequena,
                      ),
                    )),
                SizedBox(height: 2.h),
                Container(
                  height: 10.h,
                  width: 90.w,
                  decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.15),
                    borderRadius: BorderRadius.all(Radius.circular(4.w)),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        height: 10.h,
                        width: 25.w,
                        decoration: BoxDecoration(
                          color: CoresAplicativo.corPadraoAzulEscuro.withOpacity(0.2),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(4.w),
                            bottomLeft: Radius.circular(4.w),
                          ),
                        ),
                      ),
                      SizedBox(width: 3.w),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: 1.h),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: TextWidget(
                                    "Estou na",
                                    fontSize: FontesAplicativo.fonteMinuscula,
                                  )),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: TextWidget(
                                  "ONG PATINHAS PARA ADOÇÃO",
                                  fontWeight: FontWeight.w600,
                                  fontSize: FontesAplicativo.fonteMuitoPequena,
                                  color: CoresAplicativo.corPadraoAzulEscuro,
                                ),
                              ),
                              SizedBox(
                                height: 1.5.h,
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: TextWidget(
                                  "Saiba mais >>",
                                  fontWeight: FontWeight.w500,
                                  fontSize: FontesAplicativo.fonteMinuscula,
                                  color: CoresAplicativo.corPadraoAzulEscuro,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 8.h),
              ]),
            ),
          ],
        ),
      ),
    );
  }
}
