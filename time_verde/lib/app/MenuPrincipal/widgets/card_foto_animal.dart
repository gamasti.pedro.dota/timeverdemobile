import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:time_verde/main.dart';
import '../../../helpers/cores_aplicativo.dart';
import '../../../helpers/fontes_aplicativo.dart';
import '../../../models/animal/animal.dart';
import '../../shared/widgets/text_widget.dart';

class CardFotoAnimal extends StatefulWidget {
  final Animal animal;
  final int imagemAtual;
  final void Function()? onTapAvancar;
  final void Function()? onTapVoltar;
  final void Function()? onTapInfo;
  final void Function()? onLongPress;
  const CardFotoAnimal(
      {super.key,
      required this.animal,
      this.imagemAtual = 0,
      this.onTapAvancar,
      this.onTapVoltar,
      this.onTapInfo,
      this.onLongPress});

  @override
  State<CardFotoAnimal> createState() => _CardFotoAnimalState();
}

class _CardFotoAnimalState extends State<CardFotoAnimal> {
  Widget build(BuildContext context) => cardFrontal();

  Widget cardFrontal() => GestureDetector(
        child: Builder(builder: (context) {
          final provider = Provider.of<CardProvider>(context, listen: false);
          final position = provider.position;
          const milliseconds = 0;

          return AnimatedContainer(
            curve: Curves.easeInOut,
            duration: const Duration(milliseconds: milliseconds),
            transform: Matrix4.identity()..translate(position.dx, position.dy),
            child: card(),
          );
        }),
        onPanStart: (details) {
          final provider = Provider.of<CardProvider>(context, listen: false);
          provider.startposition(details);
        },
        onPanUpdate: (details) {
          final provider = Provider.of<CardProvider>(context, listen: false);

          provider.updateposition(details);
        },
        onPanEnd: (details) {
          final provider = Provider.of<CardProvider>(context, listen: false);

          provider.endposition();
        },
      );

  @override
  Widget card() => Stack(
        children: [
          Center(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(4.h)),
                image: DecorationImage(image: widget.animal.listaImagem![widget.imagemAtual].image),
              ),
              height: 76.h,
              width: 90.w,
            ),
          ),
          Center(
              child: Container(
                  margin: EdgeInsets.only(top: 56.h),
                  alignment: Alignment.bottomCenter,
                  height: 20.h,
                  width: 90.w,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(4.h)),
                    gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      // stops: [0.1, 0.5, 0.7, 0.9],
                      colors: [
                        (widget.animal.tipo == Tipo.cachorro ? CoresAplicativo.corCachorro : CoresAplicativo.corGato)
                            .withOpacity(0.90),
                        (widget.animal.tipo == Tipo.cachorro ? CoresAplicativo.corCachorro : CoresAplicativo.corGato)
                            .withOpacity(0.7),
                        (widget.animal.tipo == Tipo.cachorro ? CoresAplicativo.corCachorro : CoresAplicativo.corGato)
                            .withOpacity(0.5),
                        (widget.animal.tipo == Tipo.cachorro ? CoresAplicativo.corCachorro : CoresAplicativo.corGato)
                            .withOpacity(0.25),
                        (widget.animal.tipo == Tipo.cachorro ? CoresAplicativo.corCachorro : CoresAplicativo.corGato)
                            .withOpacity(0.1),
                        Colors.transparent,
                      ],
                    ),
                  ))),
          Padding(
            padding: EdgeInsets.only(left: 8.w, top: 69.h),
            child: TextWidget(
              widget.animal.nome,
              color: CoresAplicativo.corBranco,
              letterSpacing: 1,
              fontWeight: FontWeight.bold,
              fontSize: FontesAplicativo.fonteNomeAnimal,
            ),
          ),
          Center(
            child: SizedBox(
              height: 76.h,
              width: 90.w,
              child: GestureDetector(
                onLongPress: widget.onLongPress,
                child: Column(
                  children: [
                    Expanded(
                        flex: 7,
                        child: Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: widget.onTapVoltar,
                              ),
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: widget.onTapAvancar,
                              ),
                            ),
                          ],
                        )),
                    Expanded(
                      flex: 3,
                      child: GestureDetector(
                        onTap: widget.onTapInfo,
                        // child: Container(),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      );
}
