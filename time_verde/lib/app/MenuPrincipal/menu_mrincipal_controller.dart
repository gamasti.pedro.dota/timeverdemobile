import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:time_verde/helpers/assets_aplicativo.dart';
import 'package:time_verde/models/animal/animal.dart';

class MenuPrincipalController extends GetxController {
  late RxBool isLoading;
  late RxList<Animal> listaAnimal;
  late RxInt animalAtual;

  MenuPrincipalController() {
    isLoading = false.obs;
    listaAnimal = <Animal>[].obs;
    animalAtual = 0.obs;
    preencheListaAnimal();
  }

  void preencheListaAnimal() {
    listaAnimal.addAll([
      Animal(
          id: 0,
          nome: 'Oi, eu sou a Pipoca',
          bio:
              "Sou uma SRD achada há alguns meses na regiâo do bairro Camélias, o pessoAU da ONG acredita que eu nao fui abandonada pelos meus antigos donos. Estou a procura do meu novo tutor que pode ser você :)",
          raca: "SDR",
          cor: "Caramelo",
          porte: "Medio",
          ongId: "1",
          nascimento: DateTime(2015, 07, 21, 18, 35, 10),
          ultimaVisitaVet: DateTime(2023, 02, 23, 10, 00, 00),
          genero: Genero.masculino,
          tipo: Tipo.cachorro,
          listaImagem: [Image.asset(AssetsAplicativo.cachorroExemplo)]),
    ]);
  }
}
