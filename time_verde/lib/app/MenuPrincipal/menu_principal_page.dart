import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:time_verde/app/MenuPrincipal/animal_detalhe_page.dart';
import 'package:time_verde/app/MenuPrincipal/widgets/card_foto_animal.dart';
import 'package:time_verde/app/shared/backgrounds/background_configuracao.dart';
import 'package:time_verde/app/shared/widgets/button_widget.dart';
import 'package:time_verde/app/shared/widgets/text_widget.dart';
import 'package:time_verde/helpers/assets_aplicativo.dart';
import 'package:time_verde/helpers/fontes_aplicativo.dart';
import 'package:time_verde/models/animal/animal.dart';

import '../../helpers/cores_aplicativo.dart';
import 'menu_mrincipal_controller.dart';

class MenuPrincipalPage extends StatefulWidget {
  const MenuPrincipalPage({super.key});

  @override
  State<MenuPrincipalPage> createState() => _MenuPrincipalPageState();
}

class _MenuPrincipalPageState extends State<MenuPrincipalPage> {
  MenuPrincipalController controller = Get.put(MenuPrincipalController());

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        extendBody: true,
        backgroundColor: CoresAplicativo.corBackgroundPadrao,
        resizeToAvoidBottomInset: true,
        body: SafeArea(
          child: Stack(
            children: [
              Container(
                height: 1.h,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [
                    CoresAplicativo.corCachorro.withOpacity(0.25),
                    CoresAplicativo.corCachorro,
                    CoresAplicativo.corGato,
                    CoresAplicativo.corGato.withOpacity(0.25),
                  ],
                )),
              ),
              Column(children: [
                Padding(
                  padding: EdgeInsets.only(left: 4.w, right: 4.w, top: 3.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 1.h),
                        child: GestureDetector(
                            child: Image.asset(
                              AssetsAplicativo.iconConfig,
                              // color: CoresAplicativo.corPreto,
                              width: 4.h,
                            ),
                            onTap: () {}),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 1.h),
                        child: GestureDetector(
                            child: Image.asset(
                              AssetsAplicativo.logoTop,
                              // color: CoresAplicativo.corPreto,
                              width: 35.w,
                            ),
                            onTap: () {}),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 1.h),
                        child: GestureDetector(
                            child: Image.asset(
                              AssetsAplicativo.iconChat,
                              // color: CoresAplicativo.corPreto,
                              width: 4.h,
                            ),
                            onTap: () {}),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 2.h),
                Obx(() => CardFotoAnimal(
                    animal: controller.listaAnimal[controller.animalAtual.value],
                    onTapVoltar: () => print('1'),
                    onTapAvancar: () => print('2'),
                    onTapInfo: () =>
                        Get.to(() => AnimalDetalhePage(animal: controller.listaAnimal[controller.animalAtual.value])))),
              ]),
              Padding(
                padding: EdgeInsets.only(bottom: 2.h),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: SizedBox(
                    height: 12.h,
                    width: 85.w,
                    child: Stack(children: [
                      Container(
                        decoration: BoxDecoration(
                          color: CoresAplicativo.corBranco,
                          borderRadius: BorderRadius.all(Radius.circular(25.w)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.6),
                              blurRadius: 8.0, // soften the shadow
                              spreadRadius: 0.1, //extend the shadow
                              offset: const Offset(
                                3.0, // Move to right 5  horizontally
                                3.0,
                              ),
                            )
                          ],
                        ),
                        height: 6.25.h,
                        margin: EdgeInsets.symmetric(vertical: 2.75.h),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                SizedBox(
                                  width: 4.w,
                                ),
                                GestureDetector(
                                    child: Image.asset(
                                      AssetsAplicativo.iconNope,
                                      width: 5.h,
                                    ),
                                    onTap: () {}),
                                SizedBox(
                                  width: 4.w,
                                ),
                                GestureDetector(
                                    child: Icon(
                                      Icons.share_outlined,
                                      color: CoresAplicativo.corCinzaDesabilitado,
                                      size: 4.h,
                                    ),
                                    onTap: () {}),
                              ],
                            ),
                            Row(
                              children: [
                                GestureDetector(
                                    child: Image.asset(
                                      AssetsAplicativo.iconDetails,
                                      color: CoresAplicativo.corCinzaDesabilitado,
                                      width: 3.h,
                                    ),
                                    onTap: () {}),
                                SizedBox(
                                  width: 4.w,
                                ),
                                GestureDetector(
                                    child: Image.asset(
                                      AssetsAplicativo.iconOsso,
                                      // color: CoresAplicativo.corPreto,
                                      width: 5.h,
                                    ),
                                    onTap: () {}),
                                SizedBox(
                                  width: 4.w,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Center(
                        child: Container(
                          decoration: BoxDecoration(
                              color: CoresAplicativo.corBranco,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.6),
                                  blurRadius: 8.0,
                                  spreadRadius: 0.1,
                                  offset: const Offset(
                                    1.0,
                                    1.0,
                                  ),
                                )
                              ],
                              borderRadius: BorderRadius.all(Radius.circular(25.w))),
                          // margin: EdgeInsets.symmetric(vertical: 1.h),
                          height: 11.h,
                          width: 11.h,
                          child: Padding(
                            padding: EdgeInsets.all(2.75.h),
                            child: GestureDetector(
                                child: Image.asset(
                                  AssetsAplicativo.iconCore,
                                  // color: CoresAplicativo.corPreto,
                                  width: 5.h,
                                ),
                                onTap: () {}),
                          ),
                        ),
                      ),
                    ]),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
