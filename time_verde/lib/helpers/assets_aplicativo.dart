class AssetsAplicativo {
  static String get pngPath => 'lib/assets/png/';

  static String get iconChat => '${pngPath}ICON_CHAT.png';
  static String get iconConfig => '${pngPath}ICON_CONFIG.png';
  static String get iconCore => '${pngPath}ICON_CORE.png';
  static String get iconDetails => '${pngPath}ICON_DETAILS.png';
  static String get iconNope => '${pngPath}ICON_NOPE.png';
  static String get iconOsso => '${pngPath}ICON_OSSO.png';
  static String get logoTop => '${pngPath}LOGO_TOP.png';
  static String get cachorroExemplo => '${pngPath}cachorro.png';
  static String get imagemLogin => '${pngPath}TOPO_IMAGEM.png';
}
