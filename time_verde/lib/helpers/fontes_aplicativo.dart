import 'package:sizer/sizer.dart';

class FontesAplicativo {
  static double get fonteMinuscula => 11.sp; //rs
  static double get fonteMuitoPequena => 12.sp;
  static double get fontePequena => 13.sp;
  static double get fonteDescricao => 14.sp;
  static double get fontePadrao => 16.sp;
  static double get fonteNomeAnimalDetalhe => 17.5.sp;
  static double get fonteNomeAnimal => 19.sp;
  //  static String Verde { get { return "#"; } }
  //  static String Azul { get { return "#0000FF"; } }
  //  static String Amarelo { get { return "#FFFF00"; } }
  //  static String Roxo { get { return "#800080"; } }
  //  static String Laranja { get { return "#FFA500"; } }
  //  static String Preto { get { return "#000000"; } }
}
