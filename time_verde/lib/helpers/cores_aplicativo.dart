import 'package:flutter/services.dart';

class CoresAplicativo {
  static Color get corBackgroundPadrao => const Color(0xffF9F9F9);
  // static Color get corBackgroundCinza => const Color(0xffF5F5F5);

  static Color get corCinzaDesabilitado => const Color(0x6A646464);
  static Color get corFonteDescricao => Color.fromARGB(147, 0, 0, 0);

  static Color get corPadraoAzulEscuro => const Color(0xff156064);
  static Color get corPadraoAzulVerde => const Color(0xff00C49A);
  static Color get corPadraoAmarelo => const Color(0xffF8E16C);
  static Color get corPadraoRosa => const Color(0xffFFC2B4);
  static Color get corPadraoLaranja => const Color(0xffFB8F67);

  static Color get corFontePadrao => const Color(0xFF1A1A1A);
  static Color get corFonteHintText => const Color(0xFF1A1A1A).withOpacity(0.7);
  static Color get corPreto => const Color(0xff000000);
  static Color get corBranco => const Color(0xffFFFFFF);

  static Color get corCachorro => const Color(0xffF8E16C);
  static Color get corGato => const Color(0xffFFC2B4);

  // static Color get cor => const Color(0xff);
}
