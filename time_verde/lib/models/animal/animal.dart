import 'dart:convert';

import 'package:flutter/material.dart';

class Animal {
  final int id;
  final String nome;
  final DateTime? nascimento;
  final String raca;
  final String bio;
  final DateTime? ultimaVisitaVet;

  final String cor;
  final String porte;
  final String ongId;
  final Tipo tipo;
  final Genero genero;
  // Ong? ong;
  List<Image>? listaImagem;

  Animal({
    required this.id,
    required this.nome,
    required this.bio,
    required this.raca,
    required this.cor,
    required this.porte,
    required this.ongId,
    required this.nascimento,
    required this.ultimaVisitaVet,
    required this.genero,
    required this.tipo,
    this.listaImagem,
  });

  factory Animal.fromJson(Map<String, dynamic> json) {
    return Animal(
      id: json['id'],
      nome: json['nome'],
      raca: json['raca'],
      cor: json['cor'],
      porte: json['porte'],
      bio: json['bio'],
      ongId: json['ongId'],
      nascimento: json['nascimento'] != null ? DateTime.parse(json['nascimento']) : null,
      ultimaVisitaVet: json['ultimaVisitaVet'] != null ? DateTime.parse(json['ultimaVisitaVet']) : null,
      tipo: Tipo.values[['tipo'] as int],
      genero: Genero.values[['tipo'] as int],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nome': nome,
      'raca': raca,
      'cor': cor,
      'porte': porte,
      'bio': bio,
      'ongId': ongId,
      'nascimento': nascimento?.toIso8601String(),
      'ultimaVisitaVet': ultimaVisitaVet?.toIso8601String(),
      'tipo': tipo.index,
      'genero': genero.index,
    };
  }

  static String get nomeTabela => "Animal";

  static String get scriptCriacaoBanco => '''
    CREATE TABLE Animal (
      id INTEGER,
      nome TEXT,
      raca TEXT,
      bio TEXT,
      ongId TEXT,
      porte TEXT,
      cor TEXT,
      nascimento TEXT,
      ultimaVisitaVet TEXT,
      tipo INTEGER,
      genero INTEGER
    )
  ''';
}

enum Genero { masculino, feminino }

enum Tipo { cachorro, gato }
