class Usuario {
  String nome;
  int idade;
  String email;
  String senha;
  String bio;
  String estado;
  String cidade;
  // List<String> favoriteAnimals;

  Usuario({
    required this.nome,
    required this.idade,
    required this.email,
    required this.senha,
    required this.bio,
    required this.estado,
    required this.cidade,
    // required this.favoriteAnimals,
  });

  factory Usuario.fromJson(Map<String, dynamic> json) {
    return Usuario(
      nome: json['nome'],
      idade: json['idade'],
      email: json['email'],
      senha: json['senha'],
      bio: json['bio'],
      estado: json['estado'],
      cidade: json['cidade'],
      // favoriteAnimals: List<String>.from(json['favoriteAnimals']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'nome': nome,
      'idade': idade,
      'email': email,
      'senha': senha,
      'bio': bio,
      'estado': estado,
      'cidade': cidade,
      // 'favoriteAnimals': favoriteAnimals,
    };
  }

  static String get nomeTabela => "Usuario";

  static String get scriptCriacaoBanco => '''
    CREATE TABLE Usuario (
      id INTEGER PRIMARY KEY,
      nome TEXT,
      idade INTEGER,
      email TEXT,
      senha TEXT,
      bio TEXT,
      estado TEXT,
      cidade TEXT
    )
  ''';
}
