import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:time_verde/app/login/login_page.dart';

import 'helpers/navigator_service.dart';

void main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CardProvider(),
      child: Sizer(builder: (context, orientation, deviceType) {
        return GetMaterialApp(
          navigatorKey: NavigationService.navigatorKey, // set property
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),

          home: const LoginPage(),
        );
      }),
    );
  }
}

class CardProvider extends ChangeNotifier {
  Offset _position = Offset.zero;
  Offset get position => _position;
  void startposition(DragStartDetails details) {}
  void updateposition(DragUpdateDetails details) {
    _position += details.delta;
    notifyListeners();
  }

  void endposition() {
    _position = Offset.zero;
    notifyListeners();
  }
}
